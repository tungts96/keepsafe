package com.tungts.keepsafemedia;

import android.app.Activity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.tungts.keepsafemedia.activities.AddItemActivity;
import com.tungts.keepsafemedia.activities.DetailActivity;
import com.tungts.keepsafemedia.adapter.LinearItemAdapter;

import java.util.ArrayList;

/**
 * Created by tungts on 8/21/2017.
 */

public class ActionModeCallBack implements ActionMode.Callback {

    Activity context;
    LinearItemAdapter linearItemAdapter;
    RecyclerView recyclerView;
    ArrayList list;

    public ActionModeCallBack(Activity context, LinearItemAdapter linearItemAdapter, RecyclerView recyclerView, ArrayList list) {
        this.context = context;
        this.linearItemAdapter = linearItemAdapter;
        this.recyclerView = recyclerView;
        this.list = list;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        if (context instanceof AddItemActivity){
            mode.getMenuInflater().inflate(R.menu.menu_action_mode_add_item,menu);
        } else if (context instanceof DetailActivity){
            mode.getMenuInflater().inflate(R.menu.menu_action_mode_export_item,menu);
        }
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        if (context instanceof AddItemActivity){
            switch (item.getItemId()){
                case R.id.action_select_all:
                    ((AddItemActivity) context).selectAll();
                    break;
            }
        } else if (context instanceof DetailActivity){
            switch (item.getItemId()){
                case R.id.action_select_all:
                    ((DetailActivity) context).selectAll();
                    break;
            }
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode){
        linearItemAdapter.clearSelections();
        if (context instanceof AddItemActivity){
            ((AddItemActivity) context).setNullActionMode();
            ((AddItemActivity) context).setButtonImport();
        } else if (context instanceof DetailActivity){
            ((DetailActivity) context).setNullActionMode();
        }
    }
}
