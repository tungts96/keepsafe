package com.tungts.keepsafemedia.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.tungts.keepsafemedia.R;
import com.tungts.keepsafemedia.model.entity.Video;
import com.tungts.keepsafemedia.utils.constant.Constants;
import com.tungts.keepsafemedia.view.AlDialog;

import java.util.ArrayList;

public class VideoActivity extends AppCompatActivity {

    VideoView videoView;
    ImageView imgExport, imgShare, imgMove, imgDelete, imgRoute, imgPlay;
    Video video;
    boolean isPlay = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        imgRoute = (ImageView) findViewById(R.id.imgRoute);
        imgExport = (ImageView) findViewById(R.id.imgExport);
        imgDelete = (ImageView) findViewById(R.id.imgDelete);
        imgPlay = (ImageView) findViewById(R.id.imgPlayVideo);
        Bundle bundle = getIntent().getExtras();
        video = (Video) bundle.getSerializable("IMAGE_OR_VIDEO");
        videoView = (VideoView) findViewById(R.id.videoView);
        addEvents();
        MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        mediaController.setMediaPlayer(videoView);
        videoView.requestFocus();
        videoView.setVideoPath(video.getUri());
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgPlay.setVisibility(View.INVISIBLE);
                videoView.start();
            }
        });

    }

    private void addEvents() {
        imgExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBroadcast();
                finish();
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlDialog alDialog = new AlDialog(VideoActivity.this);
                alDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alDialog.setMessage("Are you sure?");
                alDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sendBroadcast();
                        finish();
                    }
                });
                alDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alDialog.dismiss();
                    }
                });
                alDialog.show();
            }
        });
    }

    private void sendBroadcast() {
        Intent intent = new Intent(Constants.Action.ACTION_UPDATE_EXPORT_DATA_DETAIL);
        Bundle bundle = new Bundle();
        ArrayList list = new ArrayList();
        list.add(video);
        bundle.putSerializable("ITEM", list);
        intent.putExtras(bundle);
        LocalBroadcastManager.getInstance(VideoActivity.this).sendBroadcast(intent);
    }
}
