package com.tungts.keepsafemedia.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tungts.keepsafemedia.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.tungts.keepsafemedia.adapter.LinearItemAdapter;
import com.tungts.keepsafemedia.model.FileManager;
import com.tungts.keepsafemedia.model.entity.Image;
import com.tungts.keepsafemedia.model.MediaManager;
import com.tungts.keepsafemedia.utils.constant.Constants;
import com.tungts.keepsafemedia.utils.encrypt.FileEncrypt;
import com.tungts.keepsafemedia.view.AlDialog;

import java.util.ArrayList;

public class PhotoActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    ImageView imgExport, imgShare, imgMove, imgDelete, imgRoute;
    MediaManager mediaManager;
    Image image;
    PhotoView photoViewer;

    int[] orientations = new int[]{ExifInterface.ORIENTATION_NORMAL,
            ExifInterface.ORIENTATION_ROTATE_90,
            ExifInterface.ORIENTATION_ROTATE_180,
            ExifInterface.ORIENTATION_ROTATE_270};

    int currentOrentation = 0;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        mediaManager = new MediaManager(PhotoActivity.this);
        photoViewer = (PhotoView) findViewById(R.id.photoViewer);
        imgRoute = (ImageView) findViewById(R.id.imgRoute);
        imgExport = (ImageView) findViewById(R.id.imgExport);
        imgDelete = (ImageView) findViewById(R.id.imgDelete);
        bundle = getIntent().getExtras();
        image = (Image) bundle.getSerializable("IMAGE_OR_VIDEO");
//        AsyncTask task = new AsyncTask() {
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                photoViewer.setImageResource(R.drawable.ic_album);
//            }
//
//            @Override
//            protected Object doInBackground(Object[] objects) {
//                byte[] b = null;
//                try {
//                    b =  FileEncrypt.decrypt(FileManager.readByteArrayFromFileEncrypt(image.getUri()));
////                            b = FileManager.readByteArrayFromFileEncrypt(image.getUri());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return b;
//            }
//
//            @Override
//            protected void onPostExecute(Object o) {
//                byte[] bytes = (byte[]) o;
//                Glide.with(PhotoActivity.this).asBitmap().load(bytes).into(photoViewer);
//                super.onPostExecute(o);
//            }
//        };
//        task.execute();
        Glide.with(PhotoActivity.this).asBitmap().load(image.getBytes()).into(photoViewer);
//        Glide.with(this).load(image.getUri()).into(photoViewer);
        addEvents();
    }

    private void addEvents() {
        imgExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBroadcast();
                finish();
            }
        });

        imgRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoViewer.setRotationBy(90);
                if (currentOrentation == 3) {
                    currentOrentation = 0;
                } else {
                    currentOrentation++;
                }
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlDialog alDialog = new AlDialog(PhotoActivity.this);
                alDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alDialog.setMessage("Are you sure?");
                alDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Constants.Action.ACTION_UPDATE_DELETE_DATA_DETAIL);
                        Bundle bundle = new Bundle();
                        ArrayList images = new ArrayList();
                        images.add(image);
                        bundle.putSerializable("ITEM",images);
                        intent.putExtras(bundle);
                        LocalBroadcastManager.getInstance(PhotoActivity.this).sendBroadcast(intent);
                        finish();
                    }
                });
                alDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alDialog.dismiss();
                    }
                });
                alDialog.show();
            }
        });
    }

    private void sendBroadcast() {
        Intent intent = new Intent(Constants.Action.ACTION_UPDATE_EXPORT_DATA_DETAIL);
        Bundle bundle = new Bundle();
        ArrayList images = new ArrayList();
        images.add(image);
        bundle.putSerializable("ITEM",images);
        intent.putExtras(bundle);
        LocalBroadcastManager.getInstance(PhotoActivity.this).sendBroadcast(intent);
    }

    public void rotate(int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                photoViewer.setRotationTo(0);
                break;
//            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
//                matrix.setScale(-1, 1);
//                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                photoViewer.setRotationTo(180);
                break;
//            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
//                matrix.setRotate(180);
//                matrix.postScale(-1, 1);
//                break;
//            case ExifInterface.ORIENTATION_TRANSPOSE:
//                matrix.setRotate(90);
//                matrix.postScale(-1, 1);
//                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
//            case ExifInterface.ORIENTATION_TRANSVERSE:
//                matrix.setRotate(-90);
//                matrix.postScale(-1, 1);
//                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            default:
                photoViewer.setRotationBy(0);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

    }
}
