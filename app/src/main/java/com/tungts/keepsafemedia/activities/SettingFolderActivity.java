package com.tungts.keepsafemedia.activities;

import android.app.Dialog;
import android.content.Intent;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tungts.keepsafemedia.R;
import com.tungts.keepsafemedia.model.entity.Folder;
import com.tungts.keepsafemedia.utils.constant.TypeOfFolder;

public class SettingFolderActivity extends AppCompatActivity {

    RelativeLayout rltName, rltLock, rltImg1, rltImg2, rltImg3;
    ImageView img1, img2, img3, imgStateLock,imgOfAlbum;
    Folder folder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_folder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings Folder");
        folder = getFolder();
        rltName = (RelativeLayout) findViewById(R.id.rltName);
        TextView tvNameFolder = (TextView) findViewById(R.id.tvNameFolder);
        tvNameFolder.setText(folder.getNameFolder());
        rltLock = (RelativeLayout) findViewById(R.id.rltLock);
        img1 = (ImageView) findViewById(R.id.imgChoose1);
        img2 = (ImageView) findViewById(R.id.imgChoose2);
        img3 = (ImageView) findViewById(R.id.imgChoose3);
        imgStateLock = (ImageView) findViewById(R.id.imgStateLock);
        imgOfAlbum = (ImageView) findViewById(R.id.imgOfAlbum);
        switch (folder.getImageFolder()){
            case TypeOfFolder.IMAGE:
                imgOfAlbum.setImageResource(R.drawable.ic_album);
                break;
            case TypeOfFolder.CARD:
                imgOfAlbum.setImageResource(R.drawable.ic_card);
                break;
            case TypeOfFolder.LOCK:
                imgOfAlbum.setImageResource(R.drawable.ic_lock);
                break;
            case TypeOfFolder.UNLOCK:
                imgOfAlbum.setImageResource(R.drawable.ic_unlock);
                break;
            case TypeOfFolder.VIDEO:
                imgOfAlbum.setImageResource(R.drawable.ic_video);
                break;
        }
        rltImg1 = (RelativeLayout) findViewById(R.id.rltImg1);
        rltImg2 = (RelativeLayout) findViewById(R.id.rltImg2);
        rltImg3 = (RelativeLayout) findViewById(R.id.rltImg3);
        if (folder.isLock()){
            imgStateLock.setImageResource(R.drawable.ic_lock);
            rltImg1.setEnabled(false);
            rltImg2.setEnabled(false);
            rltImg3.setEnabled(false);
            rltName.setEnabled(false);
        } else {
            if (folder.getPin() != null){
                rltImg1.setEnabled(false);
                rltImg2.setEnabled(false);
                rltImg3.setEnabled(false);
            }
            imgStateLock.setImageResource(R.drawable.ic_unlock);
        }
        addEvents();
    }

    private void addEvents() {

        rltName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNameFolder();
            }
        });

        rltLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (folder.getPin()== null) {
                    setLockFolder(0);
                } else if (folder.getPin() != null){
                    setLockFolder(1);
                }
            }
        });

        imgOfAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        rltImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setChoose(TypeOfFolder.IMAGE);
            }
        });

        rltImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setChoose(TypeOfFolder.VIDEO);

            }
        });

        rltImg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setChoose(TypeOfFolder.CARD);
            }
        });
    }

    public void setChoose(int choose) {
        folder.setImageFolder(choose);
        if (choose == TypeOfFolder.IMAGE){
            imgOfAlbum.setImageResource(R.drawable.ic_image);
            img1.setVisibility(View.VISIBLE);
            rltImg1.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.choose, null));
            img2.setVisibility(View.INVISIBLE);
            img3.setVisibility(View.INVISIBLE);
            rltImg2.setBackgroundResource(R.drawable.ripple_effect_album);
            rltImg3.setBackgroundResource(R.drawable.ripple_effect_album);
        } else if (choose == TypeOfFolder.VIDEO){
            imgOfAlbum.setImageResource(R.drawable.ic_video);
            img2.setVisibility(View.VISIBLE);
            rltImg2.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.choose, null));
            img1.setVisibility(View.INVISIBLE);
            img3.setVisibility(View.INVISIBLE);
            rltImg1.setBackgroundResource(R.drawable.ripple_effect_album);
            rltImg3.setBackgroundResource(R.drawable.ripple_effect_album);
        } else if (choose == TypeOfFolder.CARD){
            imgOfAlbum.setImageResource(R.drawable.ic_card);
            img3.setVisibility(View.VISIBLE);
            rltImg3.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.choose, null));
            img1.setVisibility(View.INVISIBLE);
            img2.setVisibility(View.INVISIBLE);
            rltImg1.setBackgroundResource(R.drawable.ripple_effect_album);
            rltImg2.setBackgroundResource(R.drawable.ripple_effect_album);
        }
    }

    private void setLockFolder(final int type) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_folder);
        dialog.show();
        TextView tv = dialog.findViewById(R.id.tv);
        TextView tvCancel = dialog.findViewById(R.id.btnCancelAdd);
        TextView tvOk = dialog.findViewById(R.id.btnOkAdd);
        final EditText edtAdd= dialog.findViewById(R.id.edtAdd);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        if (type == 0){
            tv.setText("Input Pin");
        } else {
            tv.setText("Remove Pin");
        }
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtAdd.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                if (type == 0){
                    if (edtAdd.getText().length() > 0){
                        rltImg1.setEnabled(false);
                        rltImg2.setEnabled(false);
                        rltImg3.setEnabled(false);
                        rltName.setEnabled(false);
                        Picasso.with(SettingFolderActivity.this).load(R.drawable.ic_lock).into(imgStateLock);
                        folder.setPin(edtAdd.getText().toString());
                        folder.setLock(true);
                        folder.setImageFolder(TypeOfFolder.LOCK);
                        dialog.dismiss();
                    } else {
                        Toast.makeText(SettingFolderActivity.this, "Pin is not empty!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (edtAdd.getText().toString().equals(folder.getPin())){
                        folder.setPin("null");
                        folder.setLock(false);
                        imgOfAlbum.setImageResource(R.drawable.ic_album);
                        imgStateLock.setImageResource(R.drawable.ic_unlock);
                        folder.setImageFolder(TypeOfFolder.IMAGE);
                        rltImg1.setEnabled(true);
                        rltImg2.setEnabled(true);
                        rltImg3.setEnabled(true);
                        rltName.setEnabled(true);
                        dialog.dismiss();
                    }
                }
            }
        });
    }

    private void setNameFolder() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_folder);
        dialog.show();
        TextView tv = dialog.findViewById(R.id.tv);tv.setText("Create Name Folder");
        TextView tvCancel = dialog.findViewById(R.id.btnCancelAdd);
        TextView tvOk = dialog.findViewById(R.id.btnOkAdd);
        final EditText edtAdd= dialog.findViewById(R.id.edtAdd);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtAdd.getText().length() > 0){
                    folder.setNameFolder(edtAdd.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(SettingFolderActivity.this, "Name folder can't not empty!! \nTry again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private Folder getFolder(){
        if (folder != null){
            return this.folder;
        } else {
            Bundle bundle = getIntent().getExtras();
            return (Folder) bundle.getSerializable("FOLDER");
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable("FOLDER",folder);
            intent.putExtras(bundle);
            setResult(99,intent);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable("FOLDER",folder);
            intent.putExtras(bundle);
            setResult(99,intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
