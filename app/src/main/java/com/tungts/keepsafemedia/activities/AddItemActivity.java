package com.tungts.keepsafemedia.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.tungts.keepsafemedia.ActionModeCallBack;
import com.tungts.keepsafemedia.R;
import com.tungts.keepsafemedia.adapter.LinearItemAdapter;
import com.tungts.keepsafemedia.interfaces.RecycleViewItemClick;
import com.tungts.keepsafemedia.model.entity.Bucket;
import com.tungts.keepsafemedia.model.MediaManager;
import com.tungts.keepsafemedia.utils.constant.Constants;

import java.util.ArrayList;
import java.util.List;

import static com.tungts.keepsafemedia.utils.constant.Constants.Type.TYPE_ADD_ITEM_IMAGE;
import static com.tungts.keepsafemedia.utils.constant.Constants.Type.TYPE_ADD_ITEM_VIDEO;

public class AddItemActivity extends AppCompatActivity implements RecycleViewItemClick {

    private int type;

    private RecyclerView rcvAddItem;
    private LinearItemAdapter addItemAdapter;
    private ArrayList list;

    private String idFolder, nameFolder;

    //import
    CardView importImage;
    Button importItem;

    //ActionMode Callback
    ActionMode actionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        type = getIntent().getIntExtra("TYPE", 0);
        idFolder = getIntent().getStringExtra("IDFOLDER");
        nameFolder = getIntent().getStringExtra("NAMEFOLDER");
        importImage = (CardView) findViewById(R.id.importImage);
        importItem = (Button) findViewById(R.id.btnImport);
        if (type == Constants.Type.TYPE_ADD_ITEM) {
            getSupportActionBar().setTitle("Keep Safe");
        } else {
            getSupportActionBar().setTitle("Add Items");
            importImage.setVisibility(View.VISIBLE);
            importItem.setEnabled(false);
        }

        //init recycle view by type
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                innitRecycleView(type);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                rcvAddItem.setLayoutManager(new LinearLayoutManager(AddItemActivity.this));
            }
        };
        task.execute();

    }

    private void innitRecycleView(int type) {
        rcvAddItem = (RecyclerView) findViewById(R.id.rcvAddItem);
        list = new ArrayList();
        if (type == Constants.Type.TYPE_ADD_ITEM) {
            addItemAdapter = new LinearItemAdapter(this, list);
            rcvAddItem.setAdapter(addItemAdapter);
            list.add("Images");
            list.addAll(new MediaManager(this).getAllBucketImage());
            list.add("Videos");
            list.addAll(new MediaManager(this).getAllBucketVideo());
            addItemAdapter.notifyDataSetChanged();
            addItemAdapter.setRecycleViewItemClick(this);
        } else {
            addEventImportButton();
            Bundle bundle = getIntent().getExtras();
            Bucket bucket = (Bucket) bundle.getSerializable("Bucket");
            idFolder = bundle.getString("IDFOLDER");
            nameFolder = bundle.getString("NAMEFOLDER");
            if (type == TYPE_ADD_ITEM_IMAGE) {
                list.addAll(bucket.getImages());
            } else {
                list.addAll(bucket.getVideos());
            }
            addItemAdapter = new LinearItemAdapter(this, list);
            addItemAdapter.setDetail(false);
            rcvAddItem.setLayoutManager(new LinearLayoutManager(this));
            rcvAddItem.setAdapter(addItemAdapter);
            addItemAdapter.setRecycleViewItemClick(this);
        }
    }

    private void addEventImportButton() {
        importItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Integer> listPos = addItemAdapter.getSelectedItems();
                ArrayList listImport = new ArrayList();
                for (int i=0;i<listPos.size();i++){
                    listImport.add(list.get(listPos.get(i)));
                }
                //Image image = (Image) list.get(potision);
            Intent intent = new Intent(Constants.Action.ACTION_UPDATE_ADD_DATA_DETAIL);
            Bundle bundle = new Bundle();
            bundle.putSerializable("ITEM",listImport);
            intent.putExtras(bundle);
            LocalBroadcastManager.getInstance(AddItemActivity.this).sendBroadcast(intent);
            finish();
            }
        });
    }

    @Override
    public void itemClick(final int position) {
        if (type == Constants.Type.TYPE_ADD_ITEM) {
            Intent intent = new Intent(AddItemActivity.this, AddItemActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (list.get(position) instanceof Bucket) {
                Bucket bucket = (Bucket) list.get(position);
                if (((Bucket) list.get(position)).getImages() != null) {
                    intent.putExtra("TYPE", TYPE_ADD_ITEM_IMAGE);
                } else {
                    intent.putExtra("TYPE", Constants.Type.TYPE_ADD_ITEM_VIDEO);
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable("Bucket", bucket);
                bundle.putString("IDFOLDER", idFolder);
                bundle.putString("NAMEFOLDER", nameFolder);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } else if (type == TYPE_ADD_ITEM_IMAGE) {
            enableActionMode(position);
//            Image image = (Image) list.get(potision);
//            Intent intent = new Intent(Constants.Action.ACTION_UPDATE_ADD_DATA_DETAIL);
//            Bundle bundle = new Bundle();
//            bundle.putSerializable("ITEM",image);
//            intent.putExtras(bundle);
//            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//            finish();
        } else if (type == TYPE_ADD_ITEM_VIDEO) {
            enableActionMode(position);
//            Video video = (Video) list.get(potision);
//            Intent intent = new Intent(Constants.Action.ACTION_UPDATE_ADD_DATA_DETAIL);
//            Bundle bundle = new Bundle();
//            bundle.putSerializable("ITEM",video);
//            intent.putExtras(bundle);
//            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//            finish();
        }
    }

    @Override
    public void itemLongClick(int position) {
        if (type == TYPE_ADD_ITEM_IMAGE) {
            //seclect
            enableActionMode(position);
            Toast.makeText(this, "selected", Toast.LENGTH_SHORT).show();
        } else if (type == TYPE_ADD_ITEM_VIDEO) {
            //seclect
            enableActionMode(position);
            Toast.makeText(this, "selected", Toast.LENGTH_SHORT).show();
        }
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(new ActionModeCallBack(this, addItemAdapter, rcvAddItem, list));
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        addItemAdapter.toggleSelection(position);
        int count = addItemAdapter.getSelectedItemCount();
        if (count == 0) {
            setButtonImport();
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count)+" Selected");
            actionMode.invalidate();
            importItem.setEnabled(true);
            importItem.setBackgroundDrawable(getResources().getDrawable(R.drawable.press_button_import));
            importItem.setTextColor(Color.WHITE);
        }
    }

    public void setButtonImport() {
        importItem.setEnabled(false);
        importItem.setBackgroundColor(getResources().getColor(R.color.button_import));
        importItem.setTextColor(getResources().getColor(R.color.text_button_import));
    }

    public void selectAll(){
        addItemAdapter.selectAll();
        actionMode.setTitle(String.valueOf(list.size())+" Selected");
        actionMode.invalidate();
    }

    public void setNullActionMode() {
        actionMode = null;
    }

    @Override
    public void itemClickButtonMore(int positon, ImageView imageMore) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
