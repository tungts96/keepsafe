package com.tungts.keepsafemedia.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.tungts.keepsafemedia.interfaces.UpdateBroacastReceiver;
import com.tungts.keepsafemedia.model.entity.Image;
import com.tungts.keepsafemedia.model.entity.Item;
import com.tungts.keepsafemedia.utils.constant.Constants;

import java.util.List;

/**
 * Created by tungts on 8/19/2017.
 */

public class MyBroadcastReceiver extends BroadcastReceiver {

    private UpdateBroacastReceiver onUpdateListener;

    public MyBroadcastReceiver(){

    }

    public MyBroadcastReceiver(UpdateBroacastReceiver onUpdateListener) {
        this.onUpdateListener = onUpdateListener;
    }

    public void setOnUpdateListener(UpdateBroacastReceiver onUpdateListener) {
        this.onUpdateListener = onUpdateListener;
    }

    public UpdateBroacastReceiver getOnUpdateListener() {
        return onUpdateListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Constants.Action.ACTION_UPDATE_ADD_DATA_DETAIL)){
            if (onUpdateListener != null){
                onUpdateListener.updateActivityAddItem(intent);
            }
        } else {
            if (onUpdateListener != null){
                onUpdateListener.updateActivityAddItem(intent);
            }
        }
    }

}
