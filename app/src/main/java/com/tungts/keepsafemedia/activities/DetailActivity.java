package com.tungts.keepsafemedia.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.tungts.keepsafemedia.ActionModeCallBack;
import com.tungts.keepsafemedia.R;
import com.tungts.keepsafemedia.adapter.LinearItemAdapter;
import com.tungts.keepsafemedia.interfaces.RecycleViewItemClick;
import com.tungts.keepsafemedia.interfaces.UpdateBroacastReceiver;
import com.tungts.keepsafemedia.model.entity.Folder;
import com.tungts.keepsafemedia.model.entity.Image;
import com.tungts.keepsafemedia.model.entity.Item;
import com.tungts.keepsafemedia.model.MediaManager;
import com.tungts.keepsafemedia.model.entity.Video;
import com.tungts.keepsafemedia.utils.constant.Constants;
import com.tungts.keepsafemedia.utils.constant.Constants.Action;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.tungts.keepsafemedia.utils.constant.Constants.Action.ACTION_UPDATE_ADD_DATA_DETAIL;
import static com.tungts.keepsafemedia.utils.constant.Constants.Action.ACTION_UPDATE_EXPORT_DATA_DETAIL;
import static com.tungts.keepsafemedia.utils.constant.Constants.Type.TYPE_ADD_ITEM;
import static com.tungts.keepsafemedia.utils.constant.Constants.Type.TYPE_DELETE_ITEM;
import static com.tungts.keepsafemedia.utils.constant.Constants.Type.TYPE_EXPORT_ITEM;

public class DetailActivity extends AppCompatActivity implements UpdateBroacastReceiver {

    private Folder folder;
    MyBroadcastReceiver myBroadcastReceiver;
    int currentPos;

    private RecyclerView rcvImageOfFolder;
    private LinearItemAdapter linearItemAdapter;
    private ArrayList<Image> arrImages;
    private ArrayList<Video> arrVideo;
    private ArrayList list;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    FloatingActionMenu fabMenu;
    FloatingActionButton fabTakePhoto;
    FloatingActionButton fabImportImage;
    MediaManager mediaManager;

    //Action Mode
    ActionMode actionMode;
    CardView cardView;
    BottomSheetBehavior behavior;
    boolean isSelected = false;
    ImageView imgShare, imgExport, imgMove, imgDelete;
    ArrayList<Integer> selectedPos;
    Integer[] selected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        this.folder = getFolder();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(folder.getNameFolder());
        addControls();
        //update activity
        selectedPos = new ArrayList<>();
        myBroadcastReceiver = new MyBroadcastReceiver(this);
        LocalBroadcastManager.getInstance(DetailActivity.this).registerReceiver(myBroadcastReceiver, new IntentFilter(ACTION_UPDATE_EXPORT_DATA_DETAIL));
        LocalBroadcastManager.getInstance(DetailActivity.this).registerReceiver(myBroadcastReceiver, new IntentFilter(ACTION_UPDATE_ADD_DATA_DETAIL));

        mediaManager = new MediaManager(this);
        rcvImageOfFolder = (RecyclerView) findViewById(R.id.rcvDetail);

        //load data from folder
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                arrImages = mediaManager.getImageFromNameFolder(folder.getNameFolder());
                arrVideo = mediaManager.getVideoFromNameFolder(folder.getNameFolder());
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                inniteRecycleview();
                rcvImageOfFolder.setLayoutManager(new LinearLayoutManager(DetailActivity.this));
            }
        };
        task.execute();
        addEvents();

    }

    private void addControls() {
        cardView = (CardView) findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(cardView);
        imgShare = cardView.findViewById(R.id.imgShare);
        imgExport = cardView.findViewById(R.id.imgExport);
        imgMove = cardView.findViewById(R.id.imgMove);
        imgDelete = cardView.findViewById(R.id.imgDelete);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        fabMenu = (FloatingActionMenu) findViewById(R.id.fabMenu);
        fabImportImage = (FloatingActionButton) findViewById(R.id.fabImportPhoto);
        fabTakePhoto = (FloatingActionButton) findViewById(R.id.fabTakePhoto);
    }

    private void addEvents() {
        fabImportImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailActivity.this, AddItemActivity.class);
                intent.putExtra("TYPE", TYPE_ADD_ITEM);
                intent.putExtra("IDFOLDER", folder.getIdFolder());
                intent.putExtra("NAMEFOLDER", DetailActivity.this.folder.getNameFolder());
                fabMenu.close(true);
                startActivity(intent);
                linearItemAdapter.setDetail(false);
            }
        });

        fabTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DetailActivity.this, "Take Photo", Toast.LENGTH_SHORT).show();
                fabMenu.close(true);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,9999);
            }
        });

        imgExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = (ArrayList<Integer>) linearItemAdapter.getSelectedItems();
                selected = selectedPos.toArray(new Integer[selectedPos.size()]);
                Arrays.sort(selected);
                selectedPos.clear();
                ArrayList listExport = new ArrayList();
                for (int i=(selected.length-1);i>=0;i--){
                    Log.e("Test",selected[i]+"");
                    listExport.add(list.get(selected[i]));
                    selectedPos.add(selected[i]);
                }
                actionMode.finish();
                updateDataExport(listExport);
                isSelected = false;
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = (ArrayList<Integer>) linearItemAdapter.getSelectedItems();
                selected = selectedPos.toArray(new Integer[selectedPos.size()]);
                Arrays.sort(selected);
                selectedPos.clear();
                ArrayList listExport = new ArrayList();
                for (int i = (selected.length-1); i>=0; i--){
                    listExport.add(list.get(selected[i]));
                    selectedPos.add(selected[i]);
                }
                actionMode.finish();
                updateDataDelete(listExport);
                isSelected = false;
            }
        });

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DetailActivity.this, "Share", Toast.LENGTH_SHORT).show();
            }
        });

        imgMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DetailActivity.this, "Move", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateDataExport(Integer[] selected) {

    }

    private void updateDataDelete(Integer[] selected) {

    }

    private Folder getFolder() {
        if (this.folder == null) {
            Bundle bundle = getIntent().getExtras();
            Folder folder = (Folder) bundle.getSerializable("FOLDER");
            return folder;
        } else {
            return this.folder;
        }
    }

    private void inniteRecycleview() {
        list = new ArrayList();
        list.addAll(arrImages);
        list.addAll(arrVideo);
        linearItemAdapter = new LinearItemAdapter(this, list);
        rcvImageOfFolder.setAdapter(linearItemAdapter);
        linearItemAdapter.setRecycleViewItemClick(new RecycleViewItemClick() {
            @Override
            public void itemClick(int potision) {
                fabMenu.close(true);
                if (!isSelected){
                    if (list.get(potision) instanceof Image) {
                        Intent intent = new Intent(DetailActivity.this, PhotoActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("IMAGE_OR_VIDEO", (Image) list.get(potision));
                        bundle.putInt("POS", potision);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(DetailActivity.this, VideoActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("IMAGE_OR_VIDEO", (Video) list.get(potision));
                        bundle.putInt("POS", potision);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                    currentPos = potision;
                } else {
                    enableActionMode(potision);
                }
            }

            @Override
            public void itemLongClick(int position) {
                enableActionMode(position);
            }

            @Override
            public void itemClickButtonMore(int positon, ImageView imageMore) {

            }
        });
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(new ActionModeCallBack(DetailActivity.this, linearItemAdapter, rcvImageOfFolder, list));
            isSelected = true;
            fabMenu.setVisibility(View.INVISIBLE);
            cardView.setVisibility(View.VISIBLE);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        linearItemAdapter.toggleSelection(position);
        int count = linearItemAdapter.getSelectedItemCount();
        if (count == 0) {
            actionMode.finish();
            cardView.setVisibility(View.INVISIBLE);
            fabMenu.setVisibility(View.VISIBLE);
            isSelected = false;
        } else {
            actionMode.setTitle(String.valueOf(count) + " Selected");
            actionMode.invalidate();
        }
    }

    public void selectAll(){
        linearItemAdapter.selectAll();
        actionMode.setTitle(String.valueOf(list.size())+" Selected");
        actionMode.invalidate();
    }

    public void setNullActionMode() {
        isSelected = false;
        actionMode = null;
        cardView.setVisibility(View.INVISIBLE);
        fabMenu.setVisibility(View.VISIBLE);
    }

    public void updateDataExport(List items){
        linearItemAdapter.setDetail(true);
        UpdateData task = new UpdateData(DetailActivity.this, Constants.Type.TYPE_EXPORT_ITEM);
        task.execute(items);
    }

    private void updateDataDelete(List items) {
        linearItemAdapter.setDetail(true);
        UpdateData task = new UpdateData(DetailActivity.this, Constants.Type.TYPE_DELETE_ITEM);
        task.execute(items);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9999 && resultCode == RESULT_OK){

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("DetailActivity", "start");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("DetailActivity", "pause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("DetailActivity", "Resume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myBroadcastReceiver.getOnUpdateListener() != null) {
            myBroadcastReceiver.setOnUpdateListener(null);
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myBroadcastReceiver);
        Log.e("DetailActivity", "Des");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("DetailActivity", "stop");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_more:
//                View itemMore = (View) item;
//                PopupMenu menu = new PopupMenu(this,itemMore);
//                menu.inflate(R.menu.popup_menu_detail_more);
//                menu.show();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //broadcast receiver
    @Override
    public void updateActivityAddItem(final Intent intent) {
        if (intent.getAction().equals(ACTION_UPDATE_ADD_DATA_DETAIL)) {
            Bundle bundle = intent.getExtras();
            List<Item> items = (List<Item>) bundle.getSerializable("ITEM");
            UpdateData task = new UpdateData(DetailActivity.this, TYPE_ADD_ITEM);
            linearItemAdapter.setDetail(true);
            task.execute(items);
        } else if (intent.getAction().equals(Action.ACTION_UPDATE_EXPORT_DATA_DETAIL)) {
//            Bundle bundle = intent.getExtras();
//            List<Item> items = (List<Item>) bundle.getSerializable("ITEM");
//            selectedPos.clear();
//            selectedPos.add(currentPos);
//            updateDataExport(items);
            AsyncTask task = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    Item item = (Item) list.get(currentPos);
                    if (item instanceof Image){
                        new MediaManager(DetailActivity.this).exportImage((Image) item);
                    } else {
                        new MediaManager(DetailActivity.this).exportVideo((Video) item);
                    }
                    String path = Constants.FolderKS.PATH + "/" + item.getBucketDisplayName();
                    File file = new File(path, item.getName());
                    File f2 = new File(item.getUri());
                    f2.delete();
                    if (file.exists()) {
                        file.delete();
                    }
                    return 1;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    if (((int)o) == 1){
                        list.remove(currentPos);
                        linearItemAdapter.notifyItemRemoved(currentPos);
                    }
                }
            };
            task.execute();
        } else {
//            Bundle bundle = intent.getExtras();
//            List<Item> items = (List<Item>) bundle.getSerializable("ITEM");
//            selectedPos.clear();
//            selectedPos.add(currentPos);
//            updateDataDelete(items);
            AsyncTask task = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    Item item = (Item) list.get(currentPos);
                    String path = Constants.FolderKS.PATH + "/" + item.getBucketDisplayName();
                    File file = new File(path, item.getName());
                    File f2 = new File(item.getUri());
                    f2.delete();
                    if (file.exists()) {
                        file.delete();
                    }
                    return 1;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    if (((int)o) == 1){
                        list.remove(currentPos);
                        linearItemAdapter.notifyItemRemoved(currentPos);
                    }
                }
            };
            task.execute();
        }
    }

    private class UpdateData extends AsyncTask<List<Item>, Integer, Integer> {

        Context context;
        int type;

        public UpdateData(Context context, int type) {
            this.context = context;
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(List<Item>... items) {
            List<Item> itemList = items[0];
            if (type == TYPE_ADD_ITEM) {
                //delete database
                for (Item item : itemList) {
                    String itemName = item.getFileName();
                    boolean add = mediaManager.saveItemToMyFile(item, folder.getNameFolder());
                    if (add) {
                        if (item instanceof Image) {
                            mediaManager.deleteImageById(item.getId());
                        } else {
                            mediaManager.deleteVideoById(item.getId());
                        }
                        //update folder
                        // String itemName = strings[0];
                        Log.e("Test", itemName);
                        String path = Constants.FolderKS.PATH + "/" + folder.getNameFolder() + "/" + itemName;
                        File file = new File(path);
                        if (itemName.endsWith(".mp4") || itemName.endsWith(".3gp")) {
                            Log.e("Test", "Video");
                            item = new Video();
                        } else {
                            Log.e("Test", "Image");
                            item = new Image();
                        }
                        item.setName(file.getName());
                        item.setBucketDisplayName(folder.getNameFolder());
                        item.setDateCreated(String.valueOf(file.lastModified()));
                        item.setSize(file.length());
                        item.setUri(file.getAbsolutePath());
                        list.add(item);
                        publishProgress();
                    }
                }
                return TYPE_ADD_ITEM;
            } else if (type == Constants.Type.TYPE_EXPORT_ITEM) {
                for (int i = 0;i <itemList.size();i++) {
                    Item item = itemList.get(i);
                    if (item instanceof Image) {
                        new MediaManager(context).exportImage((Image) item);
                    } else {
                        new MediaManager(context).exportVideo((Video) item);
                    }
                    boolean a = list.remove(item);
                    publishProgress(selectedPos.get(i));
                    String path = Constants.FolderKS.PATH + "/" + item.getBucketDisplayName();
                    File file = new File(path, item.getName());
                    File f2 = new File(item.getUri());
                    f2.delete();
                    if (file.exists()) {
                        file.delete();
                    }
                }
                return TYPE_EXPORT_ITEM;
            } else if(type == TYPE_DELETE_ITEM){
                for (int i = 0;i <itemList.size();i++) {
                    Item item = itemList.get(i);
                    list.remove(item);
                    publishProgress(selectedPos.get(i));
                    String path = Constants.FolderKS.PATH + "/" + item.getBucketDisplayName();
                    File file = new File(path, item.getName());
                    File f2 = new File(item.getUri());
                    f2.delete();
                    if (file.exists()) {
                        file.delete();
                    }
                }
                return TYPE_DELETE_ITEM;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (type == TYPE_ADD_ITEM) {
                linearItemAdapter.notifyItemInserted(list.size());
            } else if (type == TYPE_EXPORT_ITEM || type == TYPE_DELETE_ITEM) {
                Log.e("List",values[0] +" " +list.size());
                linearItemAdapter.notifyItemRemoved(values[0]);
            }
        }

        @Override
        protected void onPostExecute(Integer type) {
            super.onPostExecute(type);
            Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
        }
    }

}
