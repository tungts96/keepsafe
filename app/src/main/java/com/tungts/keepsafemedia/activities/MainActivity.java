package com.tungts.keepsafemedia.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tungts.keepsafemedia.R;
import com.tungts.keepsafemedia.adapter.GridAdapter;
import com.tungts.keepsafemedia.databases.Database;
import com.tungts.keepsafemedia.databases.NameDatabase;
import com.tungts.keepsafemedia.interfaces.RecycleViewItemClick;
import com.tungts.keepsafemedia.model.entity.Folder;
import com.tungts.keepsafemedia.utils.constant.Constants;
import com.tungts.keepsafemedia.model.FileManager;
import com.tungts.keepsafemedia.utils.convert.TimeConvert;
import com.tungts.keepsafemedia.utils.constant.TypeOfFolder;
import com.tungts.keepsafemedia.view.AlDialog;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rcvMain;
    private GridAdapter gridAdapter;
    private List<Folder> listFolder;
    private int currentPos;
    String oldNameFolder;

    FloatingActionButton fabAddFolder;

    private RelativeLayout rltAddFolder;
    private Button btnAddNewFolder;

    public static Database database;

    public static final  int REQUEST_CODE_SETTING = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createDatabase();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("KeepSafe");
        innitRecycleView();
        addControls();
        addEvents();

    }

    private void createDatabase(){
        database = new Database(this, "App.sqlite",null,1);

        database.queryData("CREATE TABLE IF NOT EXISTS " + NameDatabase.NameTable.TABEL_FOLDER +
                "("+ NameDatabase.NameColumn.ID_FOLDER +" TEXT NOT NULL PRIMARY KEY, " +
                NameDatabase.NameColumn.NAME_FOLDER + " TEXT NOT NULL, "+
                NameDatabase.NameColumn.DATECREATED_FOLDER + " TEXT NOT NULL, "+
                NameDatabase.NameColumn.TYPE_OF_FOLDER + " INT(10) NOT NULL, " +
                NameDatabase.NameColumn.PIN_OF_FOLDER + " TEXT NULL)");

//        database.queryData("CREATE TABLE IF NOT EXISTS " + NameDatabase.NameTable.TABEL_IMAGE_OR_VIDEO +
//                "("+ NameDatabase.NameColumn.ID +" TEXT NOT NULL PRIMARY KEY, " +
//                NameDatabase.NameColumn.URI + " TEXT NOT NULL, "+
//                NameDatabase.NameColumn.IMAGE_OR_VIDEO + " BLOB NOT NULL, "+
//                NameDatabase.NameColumn.DATE_CREATED + " TEXT NOT NULL, "+
//                NameDatabase.NameColumn.SIZE + " TEXT NOT NULL, "+
//                NameDatabase.NameColumn.BUCKET_NAME + " TEXT NOT NULL, " +
//                NameDatabase.NameColumn.ID_FOLDER_CONSTANT + " TEXT NOT NULL, " +
//                "FOREIGN KEY (" + NameDatabase.NameColumn.ID_FOLDER_CONSTANT + ") " +
//                "REFERENCES " + NameDatabase.NameTable.TABEL_FOLDER + "("+NameDatabase.NameColumn.ID_FOLDER+"))");
    }

    private void innitRecycleView() {
        rcvMain = (RecyclerView) findViewById(R.id.rcvMain);
        listFolder = database.getListFolder();
        gridAdapter = new GridAdapter(this, listFolder);
        rcvMain.setLayoutManager(new GridLayoutManager(this,2));
        rcvMain.setAdapter(gridAdapter);
    }

    private void addControls() {
        fabAddFolder = (FloatingActionButton) findViewById(R.id.fabAddFolder);
        rltAddFolder = (RelativeLayout) findViewById(R.id.rltAddFolder);
        btnAddNewFolder = (Button) findViewById(R.id.btnAddFolder);
        if (listFolder.size() > 0){
            rltAddFolder.setVisibility(View.INVISIBLE);
            fabAddFolder.setVisibility(View.VISIBLE);
        } else {
            rltAddFolder.setVisibility(View.VISIBLE);
            fabAddFolder.setVisibility(View.INVISIBLE);
        }
    }

    private void addEvents() {

        btnAddNewFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Them", Toast.LENGTH_SHORT).show();
                showDialogAddFolder();
            }
        });

        gridAdapter.setRecycleViewItemClick(new RecycleViewItemClick() {
            @Override
            public void itemClick(int potision) {
                currentPos = potision;
                if (listFolder.get(potision).isLock()){
                    checkPin(listFolder.get(potision));
                } else {
                    changeActivity(listFolder.get(potision));
                }
            }

            @Override
            public void itemLongClick(int position) {

            }

            @Override
            public void itemClickButtonMore(int positon, ImageView imageMore) {
                showPopUpMenuOfFolder(imageMore,positon);
                currentPos = positon;
            }
        });
        fabAddFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogAddFolder();
            }
        });
    }

    private void changeActivity(Folder folder){
        Intent intent = new Intent(MainActivity.this,DetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("FOLDER",folder);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void checkPin(final Folder folder) {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Album is locked");
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_folder);
        dialog.show();
        TextView tv = dialog.findViewById(R.id.tv);tv.setText("Enter the password for this album");
        TextView tvCancel = dialog.findViewById(R.id.btnCancelAdd);
        TextView tvOk = dialog.findViewById(R.id.btnOkAdd); tvOk.setText("OPEN");
        final EditText edtAdd= dialog.findViewById(R.id.edtAdd);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtAdd.getText().toString().equals(folder.getPin())){
                    changeActivity(folder);
                    folder.setLock(false);
                    folder.setImageFolder(TypeOfFolder.UNLOCK);
                    listFolder.remove(currentPos);
                    listFolder.add(currentPos,folder);
                    gridAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(MainActivity.this, "Incorrect Password", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
    }

    private void showPopUpMenuOfFolder(ImageView imageMore, final int pos) {
        PopupMenu menu = new PopupMenu(this,imageMore);
        menu.inflate(R.menu.popup_menu_folder);
        if (listFolder.get(pos).isLock()) {
            menu.getMenu().getItem(1).setVisible(false);
        }
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.mnuSettingfolder:
                        oldNameFolder = listFolder.get(pos).getNameFolder();
                        Intent intent = new Intent(MainActivity.this, SettingFolderActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("FOLDER",listFolder.get(pos));
                        intent.putExtras(bundle);
                        startActivityForResult(intent,REQUEST_CODE_SETTING);
                        break;
                    case R.id.mnuDeleteFolder:
                        deleteFolder(pos);
                        break;
                }
                return false;
            }
        });
        menu.show();
    }

    private void deleteFolder(final int pos){
        final AlDialog alDialog = new AlDialog(this);
        alDialog.setMessage("Are you sure?");
        alDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (database.deleteFolder(listFolder.get(pos))){
                    new FileManager().deleteFile(Constants.FolderKS.PATH +"/"+listFolder.get(pos).getNameFolder());
                    showToast("Đã xóa folder "+listFolder.get(pos).getNameFolder());
                    listFolder.remove(pos);
                    gridAdapter.notifyItemRemoved(pos);
                    if (listFolder.isEmpty()){
                        fabAddFolder.setVisibility(View.INVISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rltAddFolder.setVisibility(View.VISIBLE);
                            }
                        },1000);
                    }
                } else {
                    showToast("Đã có lỗi xảy ra");
                }
            }
        });
        alDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alDialog.dismiss();
            }
        });
        alDialog.show();
    }

    private void showDialogAddFolder() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_folder);
        dialog.show();
        TextView tvCancel = dialog.findViewById(R.id.btnCancelAdd);
        TextView tvOk = dialog.findViewById(R.id.btnOkAdd);
        final EditText edtAddFolder = dialog.findViewById(R.id.edtAdd);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtAddFolder.getText().length() > 0){
                    addFolder(edtAddFolder.getText().toString());
                    dialog.dismiss();
                    rltAddFolder.setVisibility(View.INVISIBLE);
                    fabAddFolder.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(MainActivity.this, "Name folder can't not empty!! \nTry again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addFolder(String nameFolder){
        if (new FileManager().addFolder(Constants.FolderKS.PATH +"/"+nameFolder)){
            Folder folder = new Folder(TimeConvert.currentTime(),nameFolder,TypeOfFolder.IMAGE);
            database.insertFolder(folder);
            listFolder.add(folder);
            gridAdapter.notifyItemInserted(listFolder.size());
        } else {
            Toast.makeText(this, "Đã có lỗi xảy ra", Toast.LENGTH_SHORT).show();
        }
    }

    private void showPopUpMenuMore() {
        View item = findViewById(R.id.menu_more);
        PopupMenu menu = new PopupMenu(this, item);
        menu.inflate(R.menu.popup_menu_more);
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.mnuSetting:
                        showToast("setting");
                        break;
                    case R.id.mnuAddFolder:
                        showDialogAddFolder();
                        break;
                    case R.id.mnuLockApp:
                        showToast("lock app");
                        break;
                }
                return false;
            }
        });
        menu.show();
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SETTING && resultCode == 99){
            Bundle bundle = data.getExtras();
            Folder folder = (Folder) bundle.getSerializable("FOLDER");
            if (!listFolder.get(currentPos).equals(folder)){
                if (database.updateFolder(folder)){
                    if (!oldNameFolder.equals(folder.getNameFolder())){
                        if (FileManager.renameFolder(oldNameFolder,folder.getNameFolder())){
                        } else {
                            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                    listFolder.remove(currentPos);
                    listFolder.add(currentPos,folder);
                    gridAdapter.notifyItemChanged(currentPos);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_more){
            showPopUpMenuMore();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }
}
