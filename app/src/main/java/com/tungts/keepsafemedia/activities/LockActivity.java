package com.tungts.keepsafemedia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.tungts.keepsafemedia.AppPreference;
import com.tungts.keepsafemedia.R;

/**
 * Created by tungts on 8/20/2017.
 */

public class LockActivity extends AppCompatActivity {

    TextView tvLock;
    PinLockView pinLockView;
    IndicatorDots indicatorDots;
    ImageView imgPin;

    String oldPin,inputPin;
    AppPreference preference;
    int MAX_LENGHT = 9;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lock_screen);
        tvLock = (TextView) findViewById(R.id.tvLock);
        imgPin = (ImageView) findViewById(R.id.imgPin);
        pinLockView = (PinLockView) findViewById(R.id.pinLockView);
        indicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        pinLockView.attachIndicatorDots(indicatorDots);
        oldPin = getOldPin();
        Toast.makeText(this, oldPin, Toast.LENGTH_SHORT).show();
        if (oldPin.isEmpty()){
            tvLock.setText("SET YOUR PIN");
            indicatorDots.setPinLength(MAX_LENGHT);
            pinLockView.setPinLength(MAX_LENGHT);
            indicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);
        } else {
            tvLock.setText("INPUT PIN");
            indicatorDots.setPinLength(oldPin.length());
            pinLockView.setPinLength(oldPin.length());
            indicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FIXED);
        }
        addEvents();
    }

    private void addEvents() {
        pinLockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                loginApp(pin);
            }

            @Override
            public void onEmpty() {

            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {
                inputPin = intermediatePin;
                Log.e("Lock",inputPin);
            }

        });

        imgPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oldPin.isEmpty()){
                    if(inputPin.isEmpty()){
                        Toast.makeText(LockActivity.this, "Pin is not Empty!! ", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.e("Lock set pin",inputPin);
                        changeScreen();
                        preference.setPin(inputPin);
                    }
                } else{
                    Toast.makeText(LockActivity.this, "Incorrect Pin", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loginApp(String pin) {
        if (pin.equals(oldPin)){
            changeScreen();
        } else if (!oldPin.isEmpty()){
            Toast.makeText(LockActivity.this, "Pin Incorrect", Toast.LENGTH_SHORT).show();
        } else {

        }
    }

    private void changeScreen(){
        Intent intent = new Intent(LockActivity.this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public String getOldPin() {
        preference = new AppPreference(this);
        return preference.getPin();
    }
}
