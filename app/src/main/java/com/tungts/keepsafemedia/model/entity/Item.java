package com.tungts.keepsafemedia.model.entity;

import android.graphics.Bitmap;

import com.tungts.keepsafemedia.utils.convert.BytesConvert;
import com.tungts.keepsafemedia.utils.convert.TimeConvert;

import java.io.Serializable;

/**
 * Created by tungts on 8/12/2017.
 */

public class Item implements Serializable{

    private String id;
    private String uri;
    private String dateCreated;
    private String bucketDisplayName;
    private long size;
    private String fileName;
    private String name;
    private byte[] bytes;

    public Item(){}

    public Item(String id, String uri, String dateCreated, String bucketDisplayName,long size) {
        this.id = id;
        this.uri = uri;
        this.dateCreated = dateCreated;
        this.bucketDisplayName = bucketDisplayName;
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDateCreated() {
        return TimeConvert.convertMilisecondsToDate(dateCreated);
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getBucketDisplayName() {
        return bucketDisplayName;
    }

    public void setBucketDisplayName(String bucketDisplayName) {
        this.bucketDisplayName = bucketDisplayName;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getFileName() {
        int index = this.uri.lastIndexOf("/");
        this.fileName = this.uri.substring(index+1,this.uri.length());
        return fileName;
    }

    public  Bitmap getImageBitmap(){
        return null;
    };

    public String getDateCreatedAndSize(){
        return BytesConvert.formatFileSize(getSize()) + ", " + getDateCreated();
    }

    public String getDateCteatedDB(){
        return this.dateCreated;
    }

    public String getDateCreatedAndSizeDB(){
        return BytesConvert.formatFileSize(getSize()) + ", " + getDateCteatedDB();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
