package com.tungts.keepsafemedia.model;


import android.util.Log;

import com.tungts.keepsafemedia.utils.encrypt.FileEncrypt;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.tungts.keepsafemedia.utils.constant.Constants.FolderKS.PATH;

/**
 * Created by tungts on 8/15/2017.
 */

public class FileManager {

    public static byte[] convertFileToByteArray(String path) {
        FileInputStream fileInputStream = null;
        byte[] bytes = null;

        try {
            File file = new File(path);
            bytes = new byte[(int) file.length()];
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytes);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bytes;
    }

    public static byte[] readByteArrayFromFileEncrypt(String path){
        BufferedInputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(convertFileToByteArray(path)));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        File file = new File(path);
        try {
            byte[] buffer = new byte[16384];
            int num ;
            int count = 0;
            while ((num=inputStream.read(buffer)) != -1){
                outputStream.flush();
                outputStream.write(buffer,0,num);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return outputStream.toByteArray();
    }

//    public static String createTempFile(File file){
//        File fTest = null;
//        try {
//            FileInputStream fileInputStream = new FileInputStream(file);
//            byte[] bytes = new byte[(int) file.length()];
//            fileInputStream.read(bytes);
//            fileInputStream.close();
//
//            fTest = File.createTempFile(file.getName(),"temp");
//            fTest.deleteOnExit();
//            long time1 = System.currentTimeMillis();
//            FileOutputStream os = new FileOutputStream(fTest.getAbsoluteFile());
//            os.write(bytes);
//            long time2 = System.currentTimeMillis();
//            Log.e("File ", (time2 - time1)+" ms");
//            os.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return fTest.getAbsolutePath();
//    }

    public boolean deleteFile(String path){
        File file = new File(path);
        if (file.exists()){
            if (file.listFiles() != null){
                for (File f : file.listFiles()){
                    if (f.exists()){
                        f.delete();
                    }
                }
            }
            file.delete();
        }
        return false;
    }

    public boolean addFolder(String path){
        File dir = new File (path);
        if (!dir.exists()){
            if (dir.mkdirs()) return true;
        }
        return false;
    }

    public static boolean renameFolder(String oldNameFolder, String newNameFolder) {
        File oldFile = new File(PATH+"/"+oldNameFolder);
        File newFile = new File(PATH+"/"+newNameFolder);
        return oldFile.renameTo(newFile);
    }
}
