package com.tungts.keepsafemedia.model.entity;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by tungts on 8/12/2017.
 */

public class Video extends Item implements Serializable {

    private Bitmap image;
    private byte[] bytes;
    private String name;

    public Video(){
        super();
    }

    public Video(String id, String uri, String dateCreated, String bucketDisplayName,long size) {
        super(id, uri, dateCreated, bucketDisplayName,size);
    }

//    @Override
//    public Bitmap getImageBitmap() {
//        Bitmap imageThumbVideo  = ThumbnailUtils.createVideoThumbnail(this.getUri(), MediaStore.Images.Thumbnails.MINI_KIND);
////            Matrix matrix = new Matrix();
////            Bitmap bmThumbnail = Bitmap.createBitmap(imageThumbVideo,0,0,imageThumbVideo.getWidth(),imageThumbVideo.getHeight(),matrix,true);
//        return imageThumbVideo;
//    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
