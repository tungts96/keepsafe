package com.tungts.keepsafemedia.model.entity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.Serializable;

/**
 * Created by tungts on 8/11/2017.
 */

public class Image extends Item implements Serializable{

    private Bitmap image;

    public Image(String id, String uri, String dateCreated, String bucketDisplayName, long size) {
        super(id, uri, dateCreated, bucketDisplayName,size);
    }

    public Image() {

    }

    @Override
    public Bitmap getImageBitmap() {
        return BitmapFactory.decodeFile(this.getUri());
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
