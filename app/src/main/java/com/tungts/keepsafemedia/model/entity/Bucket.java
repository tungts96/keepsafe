package com.tungts.keepsafemedia.model.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tungts on 8/13/2017.
 */

public class Bucket implements Serializable{

    private String path;
    private String idBucket;
    private String nameBucket;
    private int type;
    private ArrayList<Image> images;
    private ArrayList<Video> videos;

    public Bucket(String nameBucket,String path) {
        this.nameBucket = nameBucket;
        this.path = path; }

    public Bucket(String nameBucket){
        this.nameBucket = nameBucket;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }

//    public String getNameBucket(){
//        int lastIndex = this.PATH.lastIndexOf("/");
//        return PATH.substring(lastIndex+1,PATH.length());
//    }

    public String getNameBucket() {
        return nameBucket;
    }

    public String getIdBucket() {
        return idBucket;
    }

    public void setIdBucket(String idBucket) {
        this.idBucket = idBucket;
    }

    public void setNameBucket(String nameBucket) {
        this.nameBucket = nameBucket;
    }
}
