package com.tungts.keepsafemedia.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.tungts.keepsafemedia.model.entity.Bucket;
import com.tungts.keepsafemedia.model.entity.Image;
import com.tungts.keepsafemedia.model.entity.Item;
import com.tungts.keepsafemedia.model.entity.Video;
import com.tungts.keepsafemedia.utils.constant.Constants;
import com.tungts.keepsafemedia.utils.encrypt.FileEncrypt;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tungts on 8/11/2017.
 */

public class MediaManager {

    private final String TAG = "Image";
    private Context context;

    public MediaManager(Context context){
        this.context = context;
    }

    //bucket contant image
    public ArrayList<Bucket> getAllBucketImage(){
        ArrayList<Bucket> buckets = new ArrayList<>();
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor c = context.getContentResolver().query(uri,new String[]{"DISTINCT "+MediaStore.Images.Media.BUCKET_DISPLAY_NAME},
                 null,null,null);
        if (c == null || c.getCount() ==0){
            return buckets;
        }
        int indexBucketName = c.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        c.moveToFirst();
        while (!c.isAfterLast()){
            String name = c.getString(indexBucketName);
            Bucket bucket  = new Bucket(name);
            bucket.setType(Constants.Type.TYPE_ADD_ITEM_IMAGE);
            buckets.add(bucket);
            c.moveToNext();
        }
        return buckets;
    }

    //bucket contant video
    public ArrayList<Bucket> getAllBucketVideo(){
        ArrayList<Bucket> buckets = new ArrayList<>();
        Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        Cursor c = context.getContentResolver().query(uri,new String[]{"DISTINCT "+MediaStore.Video.Media.BUCKET_DISPLAY_NAME},
                null,null,null);
        if (c == null || c.getCount() ==0){
            return buckets;
        }
        int indexBucketName = c.getColumnIndex(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
        c.moveToFirst();
        while (!c.isAfterLast()){
            String name = c.getString(indexBucketName);
            Bucket bucket  = new Bucket(name);
            bucket.setType(Constants.Type.TYPE_ADD_ITEM_VIDEO);
            buckets.add(bucket);
            c.moveToNext();
        }
        return buckets;
    }

    //get list images from bucket
    public List<Image> getListImagesFromBucket(Bucket bucket){
        List<Image> images = new ArrayList<>();
        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor c = context.getContentResolver().query(uri,null,MediaStore.Images.Media.BUCKET_DISPLAY_NAME+"=?",new String[]{bucket.getNameBucket()},orderBy + " DESC");

        if (c == null || c.getCount() ==0){
            return images;
        }

        int indexName = c.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        int indexData = c.getColumnIndex(MediaStore.Images.Media.DATA);
        int indexDateCreated = c.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN);
        int indexId = c.getColumnIndex(MediaStore.Images.Media._ID);
        int indexSize = c.getColumnIndex(MediaStore.Images.Media.SIZE);
        c.moveToFirst();
        while (!c.isAfterLast()){
            String path = c.getString(indexData);
            String bucket_name = c.getString(indexName);
            String dateCreated = c.getString(indexDateCreated);
            String id = c.getString(indexId);
            long size = c.getLong(indexSize);
            Log.e(TAG,id+"\n" +path+ "\n"+bucket_name+"\n"+c.getString(indexId) + "\n" + size);
            images.add(new Image(id,path,dateCreated,bucket_name,size));
            c.moveToNext();
        }
        return images;
    }

    //get list videos from bucket
    public List<Video> getListVideosFromBucket(Bucket bucket){
        List<Video> videos = new ArrayList<>();
        final String orderBy = MediaStore.Video.Media.DATE_TAKEN;
        Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        Cursor c = context.getContentResolver().query(uri,null,MediaStore.Video.Media.BUCKET_DISPLAY_NAME+"=?",new String[]{bucket.getNameBucket()},orderBy + " DESC");

        if (c == null || c.getCount() ==0){
            return videos;
        }

        int indexBucketName = c.getColumnIndex(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
        int indexData = c.getColumnIndex(MediaStore.Video.Media.DATA);
        int indexId = c.getColumnIndex(MediaStore.Video.Media._ID);
        int indexDateCreated = c.getColumnIndex(MediaStore.Video.Media.DATE_TAKEN);
        int indexSize= c.getColumnIndex(MediaStore.Video.Media.SIZE);
        int indexMineType= c.getColumnIndex(MediaStore.Video.Media.MIME_TYPE);
        c.moveToFirst();

        while (!c.isAfterLast()){
            String path = c.getString(indexData);
            String bucket_name = c.getString(indexBucketName);
            String dateCreated = c.getString(indexDateCreated);
            String id = c.getString(indexId);
            long size = c.getLong(indexSize);
            Log.e(TAG,path+ "\n"+bucket_name+"\n"+dateCreated+"\n"+size + c.getString(indexMineType));
            videos.add(new Video(id,path,dateCreated,bucket_name,size));
            c.moveToNext();
        }
        return videos;
    }

//    //get all Image in strorage
//    public List<Image> getListImages(){
//        List<Image> images = new ArrayList<>();
//        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
//        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//        Cursor c = context.getContentResolver().query(uri,null,null,null,orderBy + " DESC");
//
//        if (c == null || c.getCount() ==0){
//            return images;
//        }
//
//        int indexName = c.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
//        int indexData = c.getColumnIndex(MediaStore.Images.Media.DATA);
//        int indexDateCreated = c.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN);
//        int indexId = c.getColumnIndex(MediaStore.Images.Media._ID);
//        int indexSize = c.getColumnIndex(MediaStore.Images.Media.SIZE);
//        c.moveToFirst();
//        while (!c.isAfterLast()){
//            String PATH = c.getString(indexData);
//            String bucket_name = c.getString(indexName);
//            String dateCreated = c.getString(indexDateCreated);
//            String id = c.getString(indexId);
//            long size = c.getLong(indexSize);
//            Log.e(TAG,PATH+ "\n"+bucket_name+"\n"+c.getString(indexId) + "\n" + size);
//            images.add(new Image(id,PATH,dateCreated,bucket_name,size));
//            c.moveToNext();
//        }
//
//        return images;
//    }
//
//    //get all video
//    public List<Video> getListVideos(){
//        List<Video> videos = new ArrayList<>();
//        final String orderBy = MediaStore.Video.Media.DATE_TAKEN;
//        Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//        Cursor c = context.getContentResolver().query(uri,null,null,null,orderBy + " DESC");
//
//        if (c == null || c.getCount() ==0){
//            return videos;
//        }
//
//        int indexBucketName = c.getColumnIndex(MediaStore.Video.Media.BUCKET_DISPLAY_NAME);
//        int indexData = c.getColumnIndex(MediaStore.Video.Media.DATA);
//        int indexId = c.getColumnIndex(MediaStore.Video.Media._ID);
//        int indexDateCreated = c.getColumnIndex(MediaStore.Video.Media.DATE_TAKEN);
//        int indexSize= c.getColumnIndex(MediaStore.Video.Media.SIZE);
//        int indexMineType= c.getColumnIndex(MediaStore.Video.Media.MIME_TYPE);
//        c.moveToFirst();
//
//        while (!c.isAfterLast()){
//            String PATH = c.getString(indexData);
//            String bucket_name = c.getString(indexBucketName);
//            String dateCreated = c.getString(indexDateCreated);
//            String id = c.getString(indexId);
//            long size = c.getLong(indexSize);
//
//            Log.e(TAG,PATH+ "\n"+bucket_name+"\n"+dateCreated+"\n"+size + c.getString(indexMineType));
//            videos.add(new Video(id,PATH,dateCreated,bucket_name,size));
//            c.moveToNext();
//        }
//        return videos;
//    }
//    //get List bucket constant image
//    public List<Bucket> getListBuckets1ContantImages(){
//        List<Bucket> listBuckets = new ArrayList<>();
//        HashMap<String,ArrayList> map = new HashMap<>();
//        ArrayList<Image>  images = (ArrayList<Image>) getListImages();
//        if (images.size() == 0) return listBuckets;
//        String s = images.get(0).getUri();
//        int lastIndex = s.lastIndexOf("/");
//        s = s.substring(0,lastIndex);
//        map.put(s,null);
//        for (int y =0;y<images.size();y++){
//            Image image = images.get(y);
//            Log.i(TAG,image.getUri());
//            String uri = image.getUri();
//            int index = uri.lastIndexOf("/");
//            uri = uri.substring(0,index);
//            if (map.get(uri) != null){
//                map.get(uri).add(image);
//                Log.i(TAG,image.getUri());
//            } else {
//                ArrayList<Image> a = new ArrayList<>();
//                a.add(image);
//                map.put(uri,a);
//            }
//        }
//        for (String urifolder : map.keySet()){
//            Bucket bucket = new Bucket(urifolder);
//            bucket.setImages(map.get(urifolder));
//            listBuckets.add(bucket);
//        }
//        return listBuckets;
//    }
//
//    //get List bucket constant Video
//    public List<Bucket> getListBuckets1ContantVideos(){
//        List<Bucket> listBuckets = new ArrayList<>();
//        HashMap<String,ArrayList<Video>> map = new HashMap<>();
//        ArrayList<Video>  videos = (ArrayList<Video>) getListVideos();
//        if (videos.size() == 0) return listBuckets;
//        String s = videos.get(0).getUri();
//        int lastIndex = s.lastIndexOf("/");
//        s = s.substring(0,lastIndex);
//        map.put(s,null);
//        for (Video video:videos){
//            String uri = video.getUri();
//            int index = uri.lastIndexOf("/");
//            uri = uri.substring(0,index);
//            if (map.get(uri) != null){
//                map.get(uri).add(video);
//            } else {
//                ArrayList<Video> a = new ArrayList<>();
//                a.add(video);
//                map.put(uri,a);
//            }
//        }
//        for (String urifolder : map.keySet()){
//            Bucket bucket = new Bucket(urifolder);
//            bucket.setVideos(map.get(urifolder));
//            listBuckets.add(bucket);
//        }
//        return listBuckets;
//    }

    //delete image by id
    public boolean deleteImageById(String idImage){
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        int succcess = context.getContentResolver().delete(uri,"_id=?",new String[]{idImage});
        if (succcess == 0){
            return false;
        }
        return true;
    }

    //delete video by id
    public boolean deleteVideoById(String idVideo){
        Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        int succcess = context.getContentResolver().delete(uri,"_id=?",new String[]{idVideo});
        if (succcess == 0){
            return false;
        }
        return true;
    }

    //export image
    public boolean exportImage(Image image)  {
        try {
            ContentValues contents = new ContentValues();
            String curTime = String.valueOf(System.currentTimeMillis());
            contents.put("title", image.getName());
            contents.put("_display_name", image.getName());
            contents.put("description", curTime);
            contents.put("datetaken", Long.valueOf(System.currentTimeMillis()));
            contents.put("mime_type", "image/jpeg");
            contents.put("_size", image.getSize());
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/public"; Log.e(TAG,path);
            String hashPath = String.valueOf(path.hashCode()); Log.e(TAG,hashPath);
            String displayName = (new File(path)).getName().toLowerCase(); Log.e(TAG,displayName);
            contents.put("bucket_id", hashPath);
            contents.put("bucket_display_name", displayName);
            contents.put("_data", path+"/"+image.getName());
            Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contents);
            BufferedOutputStream oStream = new BufferedOutputStream(context.getContentResolver().openOutputStream(uri));
            oStream.flush();
            byte[] bytes = FileManager.convertFileToByteArray(image.getUri());
            bytes = FileEncrypt.decrypt(bytes);
            BufferedInputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(bytes));
            byte[] buffer = new byte[16384];
            int numRead ;
            while((numRead = inputStream.read(buffer)) != -1){
                oStream.flush();
                oStream.write(buffer,0,numRead);
            }
            oStream.close();
//            oStream.write(bytes);
//            oStream.close();
        } catch (IOException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  true;
    }

    //save Item to My file
    public boolean saveItemToMyFile(Item item, String folder){
        File dir = new File (Constants.FolderKS.PATH + "/" +folder);
        if (!dir.exists()){
            dir.mkdirs();
        }
        File file = new File(dir, item.getFileName());
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file,true);
            BufferedOutputStream oStream = new BufferedOutputStream(fileOutputStream);
            byte[] plainBytes = FileManager.convertFileToByteArray(item.getUri());
            if (item instanceof Image){
                plainBytes = FileEncrypt.encrypt(plainBytes);
            }
            Log.e("Test",plainBytes.length+"");
            BufferedInputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(plainBytes));
            byte[] buffer = new byte[16384];
            int numRead ;
            while((numRead = inputStream.read(buffer)) != -1){
                oStream.flush();
                oStream.write(buffer,0,numRead);
//                oStream.write(FileEncrypt.encrypt(buffer),0,numRead);
            }
            oStream.close();
        } catch (FileNotFoundException e) {
            return  false;
        } catch (IOException e) {
            return  false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    //get Image from folder
    public ArrayList<Image> getImageFromNameFolder(String nameFolder){
        String path = Constants.FolderKS.PATH + "/" +nameFolder;
        ArrayList<Image> images = new ArrayList<>();
        File f = new File(path);
        if (f.listFiles() == null){
            return images;
        }
        for (File file:f.listFiles()){
            if (file.getName().endsWith(".jpg") || file.getName().endsWith(".png") ||
                    file.getName().endsWith(".jpeg") || file.getName().endsWith(".gif") ||
                    file.getName().endsWith(".bmp") || file.getName().endsWith(".webp")){
                Image image = new Image();
                image.setName(file.getName());
                image.setBucketDisplayName(nameFolder);
                image.setDateCreated(String.valueOf(file.lastModified()));
                image.setSize(file.length());
                image.setUri(file.getAbsolutePath());
                images.add(image);
            }
        }
        return images;
    }

    //get video from folder
    public ArrayList<Video> getVideoFromNameFolder(String nameFolder){
        String path = Constants.FolderKS.PATH + "/" +nameFolder;
        ArrayList<Video> videos = new ArrayList<>();
        File f = new File(path);
        if (f.listFiles() == null){
            return videos;
        }
        for (File file:f.listFiles()){
            if (file.getName().endsWith(".mp4") || file.getName().endsWith(".3gp")){
                Video video = new Video();
                video.setName(file.getName());
                video.setBucketDisplayName(nameFolder);
                video.setDateCreated(String.valueOf(file.lastModified()));
                video.setSize(file.length());
                video.setUri(file.getAbsolutePath());
                videos.add(video);
            }
        }
        return videos;
    }

    //export video
    public boolean exportVideo(Video video) {
        Log.e("ST",video.getUri());
        try {
            ContentValues contents = new ContentValues();
            String curTime = String.valueOf(System.currentTimeMillis());
            contents.put("title", curTime);
            contents.put("_display_name", curTime);
            contents.put("description", curTime);
            contents.put("datetaken", Long.valueOf(System.currentTimeMillis()));
            contents.put("mime_type", "video/mp4");
            contents.put("_size", video.getSize());
            String path = Environment.getExternalStorageDirectory().toString() + "/DCIM/public";
            String hashPath = String.valueOf(path.hashCode());
            String displayName = (new File(path)).getName().toLowerCase();
            contents.put("_data", path+"/"+video.getName());
            contents.put("bucket_id", hashPath);
            contents.put("bucket_display_name", displayName);
            Uri uri = context.getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, contents);
            BufferedOutputStream oStream = new BufferedOutputStream(context.getContentResolver().openOutputStream(uri));oStream.flush();
//            byte[] bytes = FileEncrypt.decrypt(FileManager.convertFileToByteArray(video.getUri()));
            byte[] bytes = FileManager.convertFileToByteArray(video.getUri());
            BufferedInputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(bytes));
            byte[] buffer = new byte[16384];
            int numRead ;
            int count = 0;
            while((numRead = inputStream.read(buffer)) != -1){
                oStream.flush();
                oStream.write(buffer,0,numRead);
            }
            oStream.close();
//            oStream.write(bytes);
//            oStream.close();
        } catch (IOException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

}
