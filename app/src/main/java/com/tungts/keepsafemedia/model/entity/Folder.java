package com.tungts.keepsafemedia.model.entity;

import com.tungts.keepsafemedia.utils.convert.TimeConvert;

import java.io.Serializable;

/**
 * Created by tungts on 8/11/2017.
 */

public class Folder implements Serializable {
    private String idFolder;
    private String nameFolder;
    private String dateCreated;
    private int imageFolder;
    private boolean isLock;
    private String pin;

    public Folder(String idFolder, String nameFolder, int imageFolder) {
        this.idFolder = idFolder;
        this.nameFolder = nameFolder;
        this.dateCreated = TimeConvert.convertMilisecondsToDate(idFolder);
        this.imageFolder = imageFolder;
        this.isLock = false;
    }

    public void setLock(boolean lock) {
        isLock = lock;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getIdFolder() {
        return idFolder;
    }

    public void setIdFolder(String idFolder) {
        this.idFolder = idFolder;
    }

    public String getNameFolder() {
        return nameFolder;
    }

    public void setNameFolder(String nameFolder) {
        this.nameFolder = nameFolder;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getImageFolder() {
        return imageFolder;
    }

    public void setImageFolder(int imageFolder) {
        this.imageFolder = imageFolder;
    }

    public boolean isLock() {
        return isLock;
    }

}
