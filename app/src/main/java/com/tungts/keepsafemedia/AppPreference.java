package com.tungts.keepsafemedia;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tungts.keepsafemedia.utils.constant.Constants;

/**
 * Created by tungts on 8/20/2017.
 */

public class AppPreference {

    private SharedPreferences preferences;

    public AppPreference(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setPin(String pin){
        preferences.edit().putString(Constants.Preference.KEY_PIN,pin).commit();
    }

    public String getPin(){
        return preferences.getString(Constants.Preference.KEY_PIN,"");
    }

}
