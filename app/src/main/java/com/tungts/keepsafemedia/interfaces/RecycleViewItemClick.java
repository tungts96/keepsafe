package com.tungts.keepsafemedia.interfaces;

import android.widget.ImageView;

/**
 * Created by tungts on 8/11/2017.
 */

public interface RecycleViewItemClick {

    void itemClick(int potision);
    void itemLongClick(int position);
    void itemClickButtonMore(int positon, ImageView imageMore);

}
