package com.tungts.keepsafemedia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tungts.keepsafemedia.R;
import com.tungts.keepsafemedia.model.entity.Image;
import com.tungts.keepsafemedia.utils.constant.TypeOfFolder;
import com.tungts.keepsafemedia.interfaces.RecycleViewItemClick;
import com.tungts.keepsafemedia.model.entity.Folder;

import java.util.List;

/**
 * Created by tungts on 8/11/2017.
 */

public class GridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static final int ITEM_FOLDER = 99;
    public static final int ITEM_IMAGE_OF_ALBUM = 100;

    private Context context;
    private List lists;
    private LayoutInflater layoutInflater;

    RecycleViewItemClick recycleViewItemClick;

    public GridAdapter(Context context, List list){
        this.context = context;
        this.lists = list;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_FOLDER){
            return new FolderViewHolder(layoutInflater.inflate(R.layout.item_main,null));
        } else if (viewType == ITEM_IMAGE_OF_ALBUM){
            return new ImageOfAlbumViewHolder(layoutInflater.inflate(R.layout.item_image,null));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FolderViewHolder){
            Folder album = (Folder) lists.get(position);
            ((FolderViewHolder) holder).tvNameAlbum.setText(album.getNameFolder());
            int typeOfAlbum = album.getImageFolder();
            switch (typeOfAlbum){
                case TypeOfFolder.IMAGE:
                    ((FolderViewHolder) holder).imgAlbum.setImageResource(R.drawable.ic_album);
                    break;
                case TypeOfFolder.CARD:
                    ((FolderViewHolder) holder).imgAlbum.setImageResource(R.drawable.ic_card);
                    break;
                case TypeOfFolder.LOCK:
                    ((FolderViewHolder) holder).imgAlbum.setImageResource(R.drawable.ic_lock);
                    break;
                case TypeOfFolder.UNLOCK:
                    ((FolderViewHolder) holder).imgAlbum.setImageResource(R.drawable.ic_unlock);
                    break;
                case TypeOfFolder.VIDEO:
                    ((FolderViewHolder) holder).imgAlbum.setImageResource(R.drawable.ic_video);
                    break;
            }
        } else if (holder instanceof ImageOfAlbumViewHolder){
            Image image = (Image) lists.get(position);
            ((ImageOfAlbumViewHolder) holder).img.setImageBitmap(image.getImageBitmap());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (lists.get(position) instanceof Folder){
            return ITEM_FOLDER;
        } else
        if (lists.get(position) instanceof Image) {
            return ITEM_IMAGE_OF_ALBUM;
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public RecycleViewItemClick getRecycleViewItemClick() {
        return recycleViewItemClick;
    }

    public void setRecycleViewItemClick(RecycleViewItemClick recycleViewItemClick) {
        this.recycleViewItemClick = recycleViewItemClick;
    }

    class FolderViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgAlbum,imgMore;
        private TextView tvNameAlbum;

        public FolderViewHolder(View itemView) {
            super(itemView);
            imgAlbum = itemView.findViewById(R.id.imageOfMain);
            tvNameAlbum = itemView.findViewById(R.id.tvNameAlbum);
            imgMore = itemView.findViewById(R.id.imgMore);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recycleViewItemClick != null){
                        recycleViewItemClick.itemClick(getAdapterPosition());
                    }
                }
            });
            imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recycleViewItemClick != null){
                        recycleViewItemClick.itemClickButtonMore(getAdapterPosition(),imgMore);
                    }
                }
            });
        }

    }

    class ImageOfAlbumViewHolder extends RecyclerView.ViewHolder{

        private ImageView img;

        public ImageOfAlbumViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.imgOfAlbum);
        }

    }

}
