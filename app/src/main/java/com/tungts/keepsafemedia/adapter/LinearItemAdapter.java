package com.tungts.keepsafemedia.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tungts.keepsafemedia.R;
import com.tungts.keepsafemedia.interfaces.RecycleViewItemClick;
import com.tungts.keepsafemedia.model.entity.Bucket;
import com.tungts.keepsafemedia.model.entity.Image;
import com.tungts.keepsafemedia.model.MediaManager;
import com.tungts.keepsafemedia.model.entity.Video;
import com.tungts.keepsafemedia.utils.constant.Constants;
import com.tungts.keepsafemedia.utils.convert.BytesConvert;
import com.tungts.keepsafemedia.model.FileManager;
import com.tungts.keepsafemedia.utils.encrypt.FileEncrypt;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tungts on 8/13/2017.
 */

public class LinearItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private RecycleViewItemClick recycleViewItemClick;

    private Context context;
    private List list;
    private LayoutInflater layoutInflater;
    SparseBooleanArray selectedItems;
    SparseBooleanArray chooseItems;

    boolean isDetail = true;

    public LinearItemAdapter(Context context, List list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
        selectedItems = new SparseBooleanArray();
        chooseItems = new SparseBooleanArray();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.Item.ITEM_BUCKET) {
            return new BucketViewHolder(layoutInflater.inflate(R.layout.item_bucket, null));
        } else if (viewType == Constants.Item.ITEM_TEXT) {
            return new TextViewHolder(layoutInflater.inflate(R.layout.item_text, null));
        } else if (viewType == Constants.Item.ITEM_ADD_IMAGE) {
            return new ImageViewHolder(layoutInflater.inflate(R.layout.item_detail_image, null));
        } else if (viewType == Constants.Item.ITEM_ADD_VIDEO) {
            return new VideoHolder(layoutInflater.inflate(R.layout.item_detail_image, null));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BucketViewHolder) {
            final Bucket bucket = (Bucket) list.get(position);
            if (bucket.getType() == Constants.Type.TYPE_ADD_ITEM_IMAGE) {
                AsyncTask task = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] objects) {
                        return (ArrayList<Image>) new MediaManager(context).getListImagesFromBucket(bucket);
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        bucket.setImages((ArrayList<Image>) o);
                        ((BucketViewHolder) holder).tvCountOfItem.setText(bucket.getImages().size() + "");
                        Glide.with(context).load(bucket.getImages().get(0).getUri()).into(((BucketViewHolder) holder).imgBucket);
                    }
                };
                task.execute();
                ((BucketViewHolder) holder).tvNameBucket.setText(bucket.getNameBucket());
            } else {
                AsyncTask task = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] objects) {
                        return (ArrayList<Video>) new MediaManager(context).getListVideosFromBucket(bucket);
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        bucket.setVideos((ArrayList<Video>) o);
                        ((BucketViewHolder) holder).tvCountOfItem.setText(bucket.getVideos().size() + "");
                        Glide.with(context).asBitmap().load(bucket.getVideos().get(0).getUri()).into(((BucketViewHolder) holder).imgBucket);
                    }
                };
                task.execute();
                ((BucketViewHolder) holder).tvNameBucket.setText(bucket.getNameBucket()+ " Videos");
            }

        } else if (holder instanceof TextViewHolder) {
            ((TextViewHolder) holder).tvText.setText((String) list.get(position));
        } else if (holder instanceof ImageViewHolder) {
            final Image image = (Image) list.get(position);
            ((ImageViewHolder) holder).tvNameImage.setText(image.getName());
            if (isDetail) {
                AsyncTask task = new AsyncTask() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        ((ImageViewHolder) holder).imgImage.setImageResource(R.drawable.ic_item_album);
                    }

                    @Override
                    protected Object doInBackground(Object[] objects) {
                        byte[] b = null;
                        try {
                            b =  FileEncrypt.decrypt(FileManager.readByteArrayFromFileEncrypt(image.getUri()));
//                            b = FileManager.readByteArrayFromFileEncrypt(image.getUri());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return b;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        byte[] bytes = (byte[]) o;
                        image.setBytes(bytes);
                        Glide.with(context).asBitmap().load(bytes).into(((ImageViewHolder) holder).imgImage);
                        super.onPostExecute(o);
                    }
                };
                task.execute();
                ((ImageViewHolder) holder).tvDateCreatedAndSize.setText(BytesConvert.formatFileSize(image.getSize()) + ", " + image.getDateCreated());
                holder.itemView
                        .setBackgroundColor(selectedItems.get(position) ? context.getResources().getColor(R.color.choose)
                                : Color.TRANSPARENT);
            } else {
                ((ImageViewHolder) holder).tvNameImage.setText(image.getFileName());
                Glide.with(context).load(image.getUri()).into(((ImageViewHolder) holder).imgImage);
                ((ImageViewHolder) holder).tvDateCreatedAndSize.setText(image.getDateCreatedAndSize());
                holder.itemView
                        .setBackgroundColor(selectedItems.get(position) ? context.getResources().getColor(R.color.choose)
                                : Color.TRANSPARENT);
            }
        } else if (holder instanceof VideoHolder) {
            final Video video = (Video) list.get(position);
            ((VideoHolder) holder).tvName.setText(video.getName());
            ((VideoHolder) holder).tvDateCreatedAndSize.setText("");
            if (isDetail) {
//                AsyncTask task = new AsyncTask() {
//                    @Override
//                    protected Object doInBackground(Object[] objects) {
////                        FileInputStream fileInputStream = null;
////                        try {
////                            File file = new File(video.getUri());
////                            byte[] buffer = new byte[16384];
////                            fileInputStream = new FileInputStream(file);
////                            byte[] plaintBytes = new byte[(int) file.length()];
////                            BufferedOutputStream outputStream = new BufferedOutputStream(new ByteArrayOutputStream(plaintBytes.length));
////                            int numRead;
////                            while ((numRead = fileInputStream.read(buffer)) != -1){
////                                outputStream.flush();
////                                outputStream.write(FileEncrypt.decrypt(buffer));
////                            }
////                        } catch (IOException e) {
////                            e.printStackTrace();
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                        } finally {
////                            if (fileInputStream != null) {
////                                try {
////                                    fileInputStream.close();
////                                } catch (IOException e) {
////                                    e.printStackTrace();
////                                }
////                            }
////                        }
//                        byte[] b = null;
//                        try {
////                            b =  FileEncrypt.decrypt(FileManager.readByteArrayFromFileEncrypt(video.getUri()));
////                            b = FileManager.readByteArrayFromFileEncrypt(image.getUri());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        return b;
//                    }
//
//                    @Override
//                    protected void onPostExecute(Object o) {
//                        byte[] bytes = (byte[]) o;
////                        Glide.with(context).asBitmap().load(bytes).into(((VideoHolder) holder).imgVideo);
//                        super.onPostExecute(o);
//                    }
//                };
//                task.execute();
                ((VideoHolder) holder).imgVideo.setImageResource(R.drawable.ic_video);
                ((VideoHolder) holder).tvDateCreatedAndSize.setText(BytesConvert.formatFileSize(video.getSize()) + ", " + video.getDateCreated());
                holder.itemView
                        .setBackgroundColor(selectedItems.get(position) ? context.getResources().getColor(R.color.choose)
                                : Color.TRANSPARENT);
            } else {
                ((VideoHolder) holder).tvName.setText(video.getFileName());
                Glide.with(context).load(video.getUri()).into(((VideoHolder) holder).imgVideo);
                ((VideoHolder) holder).tvDateCreatedAndSize.setText(video.getDateCreatedAndSize());
                holder.itemView
                        .setBackgroundColor(selectedItems.get(position) ? context.getResources().getColor(R.color.choose)
                                : Color.TRANSPARENT);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) instanceof Bucket) {
            return Constants.Item.ITEM_BUCKET;
        } else if (list.get(position) instanceof String) {
            return Constants.Item.ITEM_TEXT;
        } else if (list.get(position) instanceof Image) {
            return Constants.Item.ITEM_ADD_IMAGE;
        } else if (list.get(position) instanceof Video) {
            return Constants.Item.ITEM_ADD_VIDEO;
        }
        return 0;
    }

    public void selectAll() {
//        selectedItems.clear();
        for(int pos = 0;pos <list.size();pos++){
            if (!selectedItems.get(pos)){
                selectedItems.put(pos, true);
            }
        }
        notifyDataSetChanged();
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            chooseItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
            chooseItems.put(pos,true);
        }
        notifyItemChanged(pos);
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    //clear selection
    public void clearSelections() {
        selectedItems.clear();
        chooseItems.clear();
        notifyDataSetChanged();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void setDetail(boolean detail) {
        isDetail = detail;
    }

    public boolean isDetail() {
        return isDetail;
    }

    public void setRecycleViewItemClick(RecycleViewItemClick recycleViewItemClick) {
        this.recycleViewItemClick = recycleViewItemClick;
    }

    class BucketViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgBucket;
        private TextView tvNameBucket, tvCountOfItem;

        public BucketViewHolder(View itemView) {
            super(itemView);
            imgBucket = itemView.findViewById(R.id.imgBucket);
            tvNameBucket = itemView.findViewById(R.id.tvNameBucket);
            tvCountOfItem = itemView.findViewById(R.id.tvCountOfItem);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recycleViewItemClick != null) {
                        recycleViewItemClick.itemClick(getAdapterPosition());
                    }
                }
            });
        }
    }

    class TextViewHolder extends RecyclerView.ViewHolder {

        private TextView tvText;

        public TextViewHolder(View itemView) {
            super(itemView);
            tvText = itemView.findViewById(R.id.tvText);
        }
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgImage;
        private TextView tvNameImage, tvDateCreatedAndSize;

        public ImageViewHolder(final View itemView) {
            super(itemView);
            imgImage = itemView.findViewById(R.id.imgImage);
            tvDateCreatedAndSize = itemView.findViewById(R.id.tvSizeAndDateCreated);
            tvNameImage = itemView.findViewById(R.id.tvNameImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recycleViewItemClick != null) {
                        recycleViewItemClick.itemClick(getAdapterPosition());
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (recycleViewItemClick != null) {
                        recycleViewItemClick.itemLongClick(getAdapterPosition());
                    }
                    return false;
                }
            });
        }
    }

    class VideoHolder extends RecyclerView.ViewHolder {

        private ImageView imgVideo, imgVd;
        private TextView tvName, tvDateCreatedAndSize;

        public VideoHolder(View itemView) {
            super(itemView);
            imgVideo = itemView.findViewById(R.id.imgImage);
            imgVd = itemView.findViewById(R.id.vd);
            imgVd.setVisibility(View.VISIBLE);
            tvDateCreatedAndSize = itemView.findViewById(R.id.tvSizeAndDateCreated);
            tvName = itemView.findViewById(R.id.tvNameImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recycleViewItemClick != null) {
                        recycleViewItemClick.itemClick(getAdapterPosition());
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (recycleViewItemClick != null) {
                        recycleViewItemClick.itemLongClick(getAdapterPosition());
                    }
                    return false;
                }
            });
        }
    }

}
