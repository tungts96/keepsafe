package com.tungts.keepsafemedia.utils.constant;

/**
 * Created by tungts on 8/13/2017.
 */

public class Constants {

    public interface Type{
        public static final int TYPE_ADD_ITEM = 110;
        public static final int TYPE_ADD_ITEM_IMAGE = 112;
        public static final int TYPE_ADD_ITEM_VIDEO = 113;

        public static final int TYPE_EXPORT_ITEM = 111;
        public static final int TYPE_DELETE_ITEM = 119;
    }

    public interface Item{
        public static final int ITEM_BUCKET = 114;
        public static final int ITEM_TEXT = 115;
        public static final int ITEM_ADD_IMAGE = 116;
        public static final int ITEM_ADD_VIDEO = 117;
    }

    public interface FolderKS{
        public static final String PATH = android.os.Environment.getExternalStorageDirectory().getAbsolutePath()+"/KeepSafe";
    }

    public interface Action{
        public static  final String ACTION_UPDATE_ADD_DATA_DETAIL = "update";
        public static  final String ACTION_UPDATE_EXPORT_DATA_DETAIL = "export";
        public static  final String ACTION_UPDATE_DELETE_DATA_DETAIL = "export";

    }

    public interface Preference{
        public static final String KEY_PIN ="Key Pin";
    }
}
