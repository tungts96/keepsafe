package com.tungts.keepsafemedia.utils.constant;

/**
 * Created by tungts on 8/11/2017.
 */

public class TypeOfFolder {
    public static final int IMAGE = 0;
    public static final int VIDEO = 1;
    public static final int CARD = 2;
    public static final int LOCK = 3;
    public static final int UNLOCK = 4;
}
