package com.tungts.keepsafemedia.utils.convert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by tungts on 8/12/2017.
 */

public class TimeConvert {

    public static String currentTime(){
        return String.valueOf(System.currentTimeMillis());
    }

    public static String convertMilisecondsToDate(String milliSeconds){
        // Create a DateFormatter object for displaying date in specified format.
        DateFormat formatter = new SimpleDateFormat("MMM dd,yyyy");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));
        return formatter.format(calendar.getTime());
    }

//    public static void main(String[] args) {
//        System.out.println(currentTime());
//        System.out.println(convertMilisecondsToDate(currentTime()));
//    }

}