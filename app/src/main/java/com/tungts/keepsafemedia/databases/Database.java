package com.tungts.keepsafemedia.databases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.tungts.keepsafemedia.model.entity.Folder;
import com.tungts.keepsafemedia.utils.constant.TypeOfFolder;

import java.util.ArrayList;

/**
 * Created by tungts on 8/13/2017.
 */

public class Database extends SQLiteOpenHelper {



    public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    //create, update, insert, delete
    public void queryData(String sql){
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }

    //select
    public Cursor getData(String sql){
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql,null);
    }

    //insert folder
    public void insertFolder(Folder folder){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO "+ NameDatabase.NameTable.TABEL_FOLDER +" VALUES(?,?,?,?,?)";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1,folder.getIdFolder());
        statement.bindString(2,folder.getNameFolder());
        statement.bindString(3,folder.getDateCreated());
        statement.bindString(4, String.valueOf(folder.getImageFolder()));
        statement.bindString(5,"null");
        statement.executeInsert();
    }

    //update folder
    public boolean updateFolder(Folder folder){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "UPDATE "+ NameDatabase.NameTable.TABEL_FOLDER +" SET " +
                NameDatabase.NameColumn.NAME_FOLDER + "= ?, "+
                NameDatabase.NameColumn.DATECREATED_FOLDER + "= ?, " +
                NameDatabase.NameColumn.TYPE_OF_FOLDER + "= ?, "  +
                NameDatabase.NameColumn.PIN_OF_FOLDER + "= ?" +" WHERE "+
                NameDatabase.NameColumn.ID_FOLDER + "= ? ";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1,folder.getNameFolder());
        statement.bindString(2,folder.getDateCreated());
        statement.bindString(3, String.valueOf(folder.getImageFolder()));
        if (folder.getPin() != null){
            statement.bindString(4, folder.getPin());
        } else {
            statement.bindString(4, "null");
        }
        statement.bindString(5,folder.getIdFolder());
        int succ = statement.executeUpdateDelete();
        if (succ == 0){
            return false;
        }
        return true;
    }

    //delete folder (Delete Image of folder)
    public boolean deleteFolder(Folder folder){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "DELETE FROM "+ NameDatabase.NameTable.TABEL_FOLDER +" WHERE " + NameDatabase.NameColumn.ID_FOLDER + "= ? ";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1,folder.getIdFolder());
        int success  = statement.executeUpdateDelete();
        if (success == 0){
            return false;
        }
        return true;
    }

    //get all folder
    public ArrayList<Folder> getListFolder(){
        ArrayList<Folder> arrFolders = new ArrayList<>();
        Cursor cursor = getData("SELECT * FROM "+ NameDatabase.NameTable.TABEL_FOLDER);
        if (cursor == null || cursor.getCount() == 0 ){
            return arrFolders;
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            String idFolder = cursor.getString(cursor.getColumnIndex(NameDatabase.NameColumn.ID_FOLDER));
            String nameFolder = cursor.getString(cursor.getColumnIndex(NameDatabase.NameColumn.NAME_FOLDER));
            String dateCreates = cursor.getString(cursor.getColumnIndex(NameDatabase.NameColumn.DATECREATED_FOLDER));
            int typeOfFolder = cursor.getInt(cursor.getColumnIndex(NameDatabase.NameColumn.TYPE_OF_FOLDER));
            String pin = cursor.getString(cursor.getColumnIndex(NameDatabase.NameColumn.PIN_OF_FOLDER));
            Folder folder = new Folder(idFolder,nameFolder,typeOfFolder);
            if (!pin.equals("null")){
                Log.e("Test",pin);
                folder.setPin(pin);
                folder.setLock(true);
                folder.setImageFolder(TypeOfFolder.LOCK);
            } else{
                folder.setPin(null);
                folder.setLock(false);
            }
            arrFolders.add(folder);
            cursor.moveToNext();
        }
        return arrFolders;
    }

//    //get image By id
//    public Image getImageById(String id){
//        String sql = "SELECT * FROM "+ NameDatabase.NameTable.TABEL_IMAGE_OR_VIDEO + " WHERE " +
//                NameDatabase.NameColumn.ID + "= " + id;
//        Cursor c = getData(sql);
//        if (c == null || c.getCount() == 0 ){
//            return null;
//        }
//        int indexId = c.getColumnIndex(NameDatabase.NameColumn.ID);
//        int indexUri = c.getColumnIndex(NameDatabase.NameColumn.URI);
//        int indexImage = c.getColumnIndex(NameDatabase.NameColumn.IMAGE_OR_VIDEO);
//        int indexDateCreated = c.getColumnIndex(NameDatabase.NameColumn.DATE_CREATED);
//        int indexSize = c.getColumnIndex(NameDatabase.NameColumn.SIZE);
//        int indexBucket = c.getColumnIndex(NameDatabase.NameColumn.BUCKET_NAME);
//        c.moveToFirst();
//            String uri = c.getString(indexUri); Log.e("ST",uri);
//            String dateCreated = c.getString(indexDateCreated); Log.e("ST",dateCreated);
//            String bucketDisplayName = c.getString(indexBucket); Log.e("ST",bucketDisplayName);
//            long size = Long.parseLong(c.getString(indexSize)); Log.e("ST",size+"");
//            Bitmap bitmap = ImageConvert.convertByteToBitmap(c.getBlob(indexImage)); Log.e("ST","bitmap");
//            Image image = new Image(id,uri,dateCreated,bucketDisplayName,size);
//            image.setImage(bitmap);
//        return image;
//    }
//
//    //count Image Of Folder
//    public long getCountImageOfFolder(String idFolder){
//        SQLiteDatabase database = getReadableDatabase();
//        String sql = "SELECT COUNT(*) FROM " + NameDatabase.NameTable.TABEL_IMAGE_OR_VIDEO + " WHERE "+ NameDatabase.NameColumn.ID_FOLDER_CONSTANT + "=" + idFolder;
//        SQLiteStatement s = database.compileStatement(sql);
//        long count = s.simpleQueryForLong();
//        return count;
//    }
//
//    //get all image by idFolder
//    public ArrayList<Image> getListImgageByIdFolder(String idFolder){
//        SQLiteDatabase database = getReadableDatabase();
//        ArrayList<Image> arrImages = new ArrayList<>();
//        String sql = "SELECT * FROM "+ NameDatabase.NameTable.TABEL_IMAGE_OR_VIDEO + " WHERE " +
//                                NameDatabase.NameColumn.ID_FOLDER_CONSTANT + "= " + idFolder;
//        Cursor c = getData(sql);
//        if (c == null || c.getCount() == 0 ){
//            return arrImages;
//        }
//
//        int indexId = c.getColumnIndex(NameDatabase.NameColumn.ID);
//        int indexUri = c.getColumnIndex(NameDatabase.NameColumn.URI);
//        int indexImage = c.getColumnIndex(NameDatabase.NameColumn.IMAGE_OR_VIDEO);
//        int indexDateCreated = c.getColumnIndex(NameDatabase.NameColumn.DATE_CREATED);
//        int indexSize = c.getColumnIndex(NameDatabase.NameColumn.SIZE);
//        int indexBucket = c.getColumnIndex(NameDatabase.NameColumn.BUCKET_NAME);
//        c.moveToFirst();
//        while (!c.isAfterLast()){
//            String id = c.getString(indexId); Log.e("ST",id);
//            String uri = c.getString(indexUri); Log.e("ST",uri);
//            String dateCreated = c.getString(indexDateCreated); Log.e("ST",dateCreated);
//            String bucketDisplayName = c.getString(indexBucket); Log.e("ST",bucketDisplayName);
//            long size = Long.parseLong(c.getString(indexSize)); Log.e("ST",size+"");
//            Bitmap bitmap = ImageConvert.convertByteToBitmap(c.getBlob(indexImage)); Log.e("ST","bitmap");
//            Image image = new Image(id,uri,dateCreated,bucketDisplayName,size);
//            image.setImage(bitmap);
//            arrImages.add(image);
//            c.moveToNext();
//        }
//        return arrImages;
//    }
//
//    //insert Image
//    public boolean insertImage(Image image, String idFolder){
//        SQLiteDatabase database = getWritableDatabase();
//        String sql = "INSERT INTO "+ NameDatabase.NameTable.TABEL_IMAGE_OR_VIDEO +" VALUES(?,?,?,?,?,?,?)";
//        SQLiteStatement statement = database.compileStatement(sql);
//        statement.clearBindings();
//
//        statement.bindString(1,image.getId());
//        statement.bindString(2,image.getUri());
//        statement.bindBlob(3, ImageConvert.convertBitmapToByteArray(image.getImageBitmap()));
//        statement.bindString(4, image.getDateCreated());
//        statement.bindString(5, String.valueOf(image.getSize()));
//        statement.bindString(6, image.getBucketDisplayName());
//        statement.bindString(7, idFolder);
//
//        long success = statement.executeInsert();
//        if (success == 0){
//            return  false;
//        }
//        return  true;
//    }
//
//    //delete Image By FolderId
//    public boolean deleteImage(String idFolder){
//        SQLiteDatabase database = getWritableDatabase();
//        String sql = "DELETE FROM "+ NameDatabase.NameTable.TABEL_IMAGE_OR_VIDEO +" WHERE " + NameDatabase.NameColumn.ID_FOLDER_CONSTANT + "= ? ";
//        SQLiteStatement statement = database.compileStatement(sql);
//        statement.clearBindings();
//
//        statement.bindString(1,idFolder);
//        int success  = statement.executeUpdateDelete();
//        if (success == 0){
//            return false;
//        }
//        return true;
//    }
//
//    //delete Image By Id
//    public boolean deleteImageById(String id){
//        SQLiteDatabase database = getWritableDatabase();
//        String sql = "DELETE FROM "+ NameDatabase.NameTable.TABEL_IMAGE_OR_VIDEO +" WHERE " + NameDatabase.NameColumn.ID + "= ? ";
//        SQLiteStatement statement = database.compileStatement(sql);
//        statement.clearBindings();
//
//        statement.bindString(1,id);
//        int success  = statement.executeUpdateDelete();
//        if (success == 0){
//            return false;
//        }
//        return true;
//    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
