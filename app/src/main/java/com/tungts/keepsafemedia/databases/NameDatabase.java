package com.tungts.keepsafemedia.databases;

/**
 * Created by tungts on 8/13/2017.
 */

public class NameDatabase {

    public interface NameTable{
        public static final String TABEL_FOLDER = "Folder";
        public static final String TABEL_IMAGE_OR_VIDEO = "ImageAndVideo";
        public static final String TABEL_VIDEO = "Video";
    }

    public interface NameColumn{

        //Table Folder
        public static final String ID_FOLDER = "IdFolder";
        public static final String NAME_FOLDER = "NameFolder";
        public static final String DATECREATED_FOLDER = "DateCreatedFolder";
        public static final String TYPE_OF_FOLDER = "TypeOfFolder";
        public static final String PIN_OF_FOLDER = "PinOfFolder";



        //Table
        public static final String ID = "Id";
        public static final String URI = "Uri";
        public static final String DATE_CREATED = "DateCreated";
        public static final String BUCKET_NAME = "BucketName";
        public static final String SIZE = "Size";
        public static final String ID_FOLDER_CONSTANT = "IdFolder";
        public static final String IMAGE_OR_VIDEO = "ImageOrVideo";
        public static final String TYPE = "Type";
    }

}
